package com.example.redisDemo;

import com.example.redisDemo.entity.PersonalInformation;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RedisDemoApplicationTests {

	@Autowired
	private RedisTemplate redisTemplate;

	@Test
	public void testForRedisTemplate000() {
		redisTemplate.opsForValue().set("123456", new PersonalInformation("name", "id"), 60, TimeUnit.SECONDS);
		Assert.assertEquals("name", ((PersonalInformation) redisTemplate.opsForValue().get("123456")).getName());
	}

	@Test
	public void testForRedisTemplate001() throws InterruptedException {
		Thread.sleep(60000);
		Assert.assertEquals(null, redisTemplate.opsForValue().get("123456"));
	}
}
