package com.example.redisDemo.DTO;

import com.example.redisDemo.entity.PersonalInformation;

import java.io.Serializable;

public class KeyValuePair implements Serializable {

    private String certifyId;
    private PersonalInformation personalInformation;

    public String getCertifyId() {
        return certifyId;
    }

    public void setCertifyId(String certifyId) {
        this.certifyId = certifyId;
    }

    public PersonalInformation getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(PersonalInformation personalInformation) {
        this.personalInformation = personalInformation;
    }

    public KeyValuePair(String certifyId, PersonalInformation personalInformation) {
        this.certifyId = certifyId;
        this.personalInformation = personalInformation;
    }

    public KeyValuePair() {
    }
}
