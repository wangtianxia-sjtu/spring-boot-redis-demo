package com.example.redisDemo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class PersonalInformation implements Serializable {

    private String name;

    private String idNo;

    public PersonalInformation() {
    }

    public String getIdNo() {
        return idNo;
    }

    public PersonalInformation(String name, String idNo) {
        this.name = name;
        this.idNo = idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Boolean isComplete() {
        return name != null && idNo != null;
    }

}
