package com.example.redisDemo.controller;

import com.example.redisDemo.DTO.KeyValuePair;
import com.example.redisDemo.entity.PersonalInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class simpleController {

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/add")
    public ResponseEntity<?> addPersonalInformation(@RequestBody KeyValuePair keyValuePair) {
        try {
            redisTemplate.opsForValue().set(keyValuePair.getCertifyId(), keyValuePair.getPersonalInformation(), 60, TimeUnit.SECONDS);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping("/query")
    public ResponseEntity<?> queryPersonalInformation(@RequestBody String certifyId) {
        try {
            PersonalInformation personalInformation = (PersonalInformation) redisTemplate.opsForValue().get(certifyId);
            if (personalInformation == null)
                throw new Exception("Relative personal information not found");
            return new ResponseEntity<>(personalInformation, HttpStatus.OK);
        }
        catch (Exception e) {
            if (!e.getMessage().equals("Relative personal information not found"))
                e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
